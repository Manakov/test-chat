var protobuf = require('protobufjs')
var path = require('path')
var webpack = require('webpack')
var express = require('express')
var socketio = require('socket.io')
var config = require('./webpack.config')
var {SERVER_URL, SERVER_PORT} = require('./config')

var app = express()
var compiler = webpack(config)

app.use(require('webpack-dev-middleware')(compiler, {
    publicPath: config.output.publicPath
}))

app.use(require('webpack-hot-middleware')(compiler))

var server = app.listen(SERVER_PORT, function(err) {
      if (err) {
        return console.error(err)
      }

      console.log('Listening at ' + SERVER_URL + ':' + SERVER_PORT)
})

var io = socketio.listen(server)

app.use('/static', express.static(__dirname + '/public'));

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, 'index.html'))
});

const protoSchema = protobuf.Root.fromJSON({
    nested: {
        ChatMessage: {
            fields: {
                text : {
                    type: "string",
                    id: 1
                },
                userName : {
                    type: "string",
                    id: 2
                }
            }
        }
    }
})

const messageProtoEncoder = protoSchema.lookup("ChatMessage")

io.on('connection', function(socket) {
      console.log('New socket connection!')

      socket.on('disconnect', function() {
            console.log('Client disconnected')
      })

      socket.on('new message', function(message) {
            let decodeMessage = messageProtoEncoder.decode(message)

            console.log('new message :', decodeMessage)

            io.emit('new message', message)
      })
})
