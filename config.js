module.exports = {
    SERVER_URL  : 'http://localhost',
    SERVER_PORT : '8000',
    MAX_MESSAGES_COUNT: 500,
    MAX_MESSAGE_LENGTH: 1024,
    MAX_USER_NAME_LENGTH: 128
};
