import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import io from 'socket.io-client';

import * as Actions from '../actions/Actions';
import { MessagesList } from '../components';
import { NewMessageForm } from '../components';
import { LoginForm } from '../components';

import { SERVER_URL, SERVER_PORT} from '../../config';

export const socket = io(SERVER_URL + ':' + SERVER_PORT);

@connect(state => ({
    messageslist: state.messageslist
}))

export default class ChatApp extends Component {

  static propTypes = {
      dispatch: PropTypes.func.isRequired,
      messageslist: PropTypes.object.isRequired,
  }

  componentDidMount() {
      const actions = bindActionCreators(Actions, this.props.dispatch);

      socket.on('new message', (message, userName) => {
          actions.addMessage(message, userName);
      });
  }

  render () {
      const { messageslist: { messages, userName }, dispatch } = this.props;
      const actions = bindActionCreators(Actions, dispatch);

      if(!userName) {
          return <LoginForm setUserName={actions.setUserName} />
      }

      return (
          <div className="chat">
              <MessagesList messages={messages} actions={actions} />
              <NewMessageForm createMessage={actions.createMessage} />
          </div>
      );
  }
}
