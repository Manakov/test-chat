import * as types from '../constants/ActionTypes';

export function createMessage(text) {
    return {
        type: types.CREATE_MESSAGE,
        text
    };
}

export function addMessage(message) {
    return {
        type: types.ADD_MESSAGE,
        message
    };
}

export function setUserName(name) {
    return {
        type: types.SET_USER_NAME,
        name
    };
}
