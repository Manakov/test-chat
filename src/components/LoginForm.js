import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';

import {MAX_USER_NAME_LENGTH} from '../../config';

export default class LoginForm extends Component {
    static propTypes = {
        setUserName: PropTypes.func.isRequired
    }

    render () {
        return (
            <form onSubmit={this.handleSubmit.bind(this)} className="login">
                <label>
                    Enter your name:
                    <input type="text"
                           autoFocus="true"
                           maxLength={MAX_USER_NAME_LENGTH}
                           className={classnames('form-control', 'new-message')}
                           value={this.state.messageContent}
                           onChange={this.handleChange.bind(this)}
                           onKeyDown={this.handleChange.bind(this)} />
                </label>
                <button type="submit">Login</button>
            </form>
        )
    }

    constructor (props, context) {
      super(props, context)
      this.state = {
          userName: this.props.userName || ''
      }
    }

    handleChange (e) {
        if (e.which === 13) {
            this.handleSubmit (e)
        }
        else {
            this.setState({userName: e.target.value })
        }
    }

    handleSubmit (e) {
        e.preventDefault();
        this.setUserName();
    }

    setUserName () {
        let name = this.state.userName,
            trimmedName = name && name.trim();

        if(!(trimmedName && trimmedName.length && trimmedName.length <= MAX_USER_NAME_LENGTH)) { return; }

        this.props.setUserName(trimmedName)
        this.setState({userName: ''})
    }
}
