import React, { Component, PropTypes } from 'react';
import Message from './Message';

export default class MessagesList extends Component {
  static propTypes = {
      messages: PropTypes.array.isRequired,
      actions: PropTypes.object.isRequired
  };

  render () {
      return (
          <ul className="messages-list">
            {this.props.messages.map(message =>
                <Message key={message.id}
                         id={message.id}
                         userName={message.userName}
                         text={message.text}
                    {...this.props.actions} />
            )}
          </ul>
      );
  }
}
