import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';

import {MAX_MESSAGE_LENGTH} from '../../config';

export default class NewMessageForm extends Component {
    static propTypes = {
        createMessage: PropTypes.func.isRequired
    }

    render () {
        return (
            <form onSubmit={this.handleSubmit.bind(this)} className="new-message">
                <input type="text"
                       autoFocus="true"
                       maxLength={MAX_MESSAGE_LENGTH}
                       className={classnames('form-control', 'new-message')}
                       value={this.state.messageContent}
                       onChange={this.handleChange.bind(this)}
                       onKeyDown={this.handleChange.bind(this)} />
                <button type="submit">Send</button>
            </form>
        )
    }

    constructor (props, context) {
        super(props, context)
        this.state = {
            messageContent: this.props.messageContent || ''
        }
    }

    handleChange (e) {
        if (e.which === 13) {
            this.handleSubmit (e)
        }
        else {
            this.setState({messageContent: e.target.value })
        }
    }

    handleSubmit (e) {
        e.preventDefault()
        this.createMessage()
    }

    createMessage () {
        let text = this.state.messageContent,
            trimmedText = text && text.trim();

        if(!(trimmedText && trimmedText.length && trimmedText.length <= MAX_MESSAGE_LENGTH)) { return; }

        this.props.createMessage(trimmedText)
        this.setState({messageContent: ''})
    }
}
