import React, { Component, PropTypes } from 'react';

export default class Message extends Component {
    static propTypes = {
        id: PropTypes.number.isRequired,
        text: PropTypes.string.isRequired
    };

    render () {
        const {props} = this;

        return (
            <li className="item">
                <div className="user-name">{props.userName}</div>
                <div className="content">{props.text}</div>
            </li>
        );
    }
}
