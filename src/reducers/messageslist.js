import * as types from '../constants/ActionTypes';
import { socket } from '../containers/ChatApp';
import protobuf from 'protobufjs';

import {MAX_MESSAGES_COUNT} from '../../config';

const initialState = {
    userName: null,
    messages: []
}

const protoSchema = protobuf.Root.fromJSON({
    nested: {
        ChatMessage: {
            fields: {
                text : {
                    type: "string",
                    id: 1
                },
                userName : {
                    type: "string",
                    id: 2
                }
            }
        }
    }
})

const messageProtoEncoder = protoSchema.lookup("ChatMessage")

let messagesCount = 0

export default function messages(state = initialState, action) {


    switch (action.type) {
          case types.CREATE_MESSAGE:
              let buffer = messageProtoEncoder.encode({
                  text : action.text,
                  userName: state.userName
              }).finish()

              socket.emit('new message', [].slice.call(buffer))
              return state;

          case types.ADD_MESSAGE:
              let message = messageProtoEncoder.decode(new Uint8Array(action.message)),
                  messages = state.messages.concat({
                      id: messagesCount++,
                      text: message.text,
                      userName: message.userName
                  })

              if(messages.length > MAX_MESSAGES_COUNT) {
                  messages = messages.splice(messages.length - MAX_MESSAGES_COUNT)
              }

              return {
                  ...state,
                  messages: messages
              };

          case types.SET_USER_NAME:
              return {
                  ...state,
                  userName: action.name
              };

          default:
              return state;
    }
}
